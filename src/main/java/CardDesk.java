import java.util.Arrays;
import java.util.Random;

public class CardDesk {
    private Card[] cards;
    private static CardDesk instance;

    private CardDesk(int count) {
        cards = new Card[count];
        int i = 0;
        for (Suits suit : Suits.values()) {
            for (Weights weight : Weights.values()) {
                this.cards[i] = new Card(weight, suit);
                i++;
            }
        }
    }

    public static synchronized CardDesk initCardDesk() {
        if (instance == null) {
            instance = new CardDesk(36);
        } else {
            instance.setCards(36);
        }
        return instance;
    }

    private void setCards(int count) {
        cards = new Card[count];
    }

    public void swap() {
        Random random = new Random(System.currentTimeMillis());
        int count = random.nextInt(100);
        for (int current = 0; current < count; current++) {
            Random rmd = new Random(System.currentTimeMillis());
            for (int bound = 0; bound<cards.length; bound++){
                    int tmpPos = rmd.nextInt(cards.length - bound);
                    Card cardTmp1 = cards[tmpPos];
                    Card cardTmp2 = cards[cards.length - 1 - bound];
                    cards[tmpPos] = cardTmp2;
                    cards[cards.length - 1 - bound] = cardTmp1;
            }
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(cards);
    }
}
