public class Card {
    private final Weights weight;
    private final Suits sute;
    private int id = 0;

    public Card(Weights weight, Suits sute) {
        this.weight = weight;
        this.sute = sute;
        this.id++;
    }

    @Override
    public String toString() {
        return "Card: " + this.weight + " " + this.sute + " ";
    }
}
