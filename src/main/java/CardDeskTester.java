public class CardDeskTester {
    public static void main(String[] args) {
        CardDesk cardDesk = CardDesk.initCardDesk();
        System.out.println(cardDesk.toString());
        cardDesk.swap();
        System.out.println(cardDesk.toString());
    }
}
